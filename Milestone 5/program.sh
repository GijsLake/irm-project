#!/bin/bash

value=$(<input.txt)
echo "$value" | grep -E -o '\w+' | grep -w '[dD]e' | wc -l

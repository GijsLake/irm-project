#!/usr/bin/python3
# File name: csv_sorter.py
# This program sorts csv files
# Author: G. Lakeman (S3383180)
# Date: 13-03-2019

import sys
import csv


def main(argv):
    if len(argv) > 1:
        reader = csv.reader(open(argv[1]))
        row_number = int(argv[2])
        sorted_list = sorted(reader, key=lambda row: row[row_number], reverse=True)
        for line in sorted_list:
            out = ''
            for item in line:
                out = out + '{0}, '.format(item)
            out = out.rstrip(',')
            print (out)
            
    else:
        print("Please give a file name argument")
        quit

if __name__ == "__main__":
    main(sys.argv)

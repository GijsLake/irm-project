#!/usr/bin/python3
# File name: tweettest.py
# creates creates a list of information about a neighborhood in csv style so when the user uses > output.csv, it will result in a usable csv file.
# This list will contain the name, the amount of lights and crime reports, the crime reports and lights per inhabitant and if the rates of lights and crimes is high.
# Author: G. Lakeman (S3383180)
# Date: 27-03-2019

import sys
import csv
import pandas as pd
import math

def main(argv):
    police_dict = {}
    light_dict = {}
    inhab_dict = {}
    area_dict = {}
    if len(argv) > 3:
        # I chose to use the csv module instead of pandas for the police report file as it was easier to traverse rows.
        with open(argv[2], 'r') as csvfile2:
            street_lights = pd.read_csv(argv[1], usecols = ['OMSCHRIJVD'])
            police_reports = csv.reader(csvfile2, skipinitialspace=True)
            inhab = pd.read_csv(argv[3], usecols = ['buurt', 'populatie', 'dichtheid'])
            for row in police_reports:
                # If everything goes to plan the error neighborhood will not be added as it will be replaced by a real neighborhood
                # If it will appear, then I know something went wrong
                hood = 'error'
                report_sum = 0
                # Here i check wheter the item is a digit or not, a digit represents a crime report and a string the neighborhood
                for item in row:
                    if item.isdigit():
                        report_sum += int(item)
                    elif not item == '-' and not item == '':
                        hood = item
                police_dict[hood] = report_sum
            
            # Create a dictionary 
            for index, row in street_lights.iterrows():
                # Add 1 to the value if the key (neighborhood) already exists
                if row['OMSCHRIJVD'] in light_dict and row['OMSCHRIJVD'] != 'nan':
                    light_dict[row['OMSCHRIJVD']] += 1
                # Create a key in the lights dictonary if there is none and make it's value 1 as long as it's value is there
                elif row['OMSCHRIJVD'] != 'nan':
                    light_dict[row['OMSCHRIJVD']] = 1
            
            # Create a dictionary with the inhabitants per neighborhood and skip all that have no value, as we can't do anything with these anyways, see report for further explanation
            for index, row in inhab.iterrows():
                if  not math.isnan(float(row['populatie'])):
                    inhab_dict[row['buurt']] = int(row['populatie'])
            
            # Create a dictionary with the density per neighborhood and skip all that have no value, as we can't do anything with these anyways, see report for further explanation
            for index, row in inhab.iterrows():
                if not math.isnan(row['populatie']) and not math.isnan(row['dichtheid']):
                    area_dict[row['buurt']] = float(row['populatie'])/float(row['dichtheid'])
            
            # First print a list of descriptons for each column then start printing information
            print('Neighboorhood, lights, crime reports, inhabitants, area, lights density, crime rates')
            for key, value in police_dict.items():
                if key in light_dict and key in inhab_dict and key in area_dict:
                    # I made the threshold 100, see report for further explanation.
                    if inhab_dict[key] > 100:
                        print ('{0}, {1}, {2}, {3}, {4}, {5}, {6}'.format(key, light_dict[key], (value/4), inhab_dict[key], area_dict[key], light_dict[key]/area_dict[key], (value/4)/inhab_dict[key]))
                        
    else:
        print("Please give file name arguments in the order: street_lights.csv police_reports.csv inhabitants.csv" )
        quit
    
    

if __name__ == "__main__":
    main(sys.argv)
